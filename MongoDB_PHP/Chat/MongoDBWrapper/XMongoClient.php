<?php

require_once dirname(__DIR__) . '/Debugging/EchoResult.php';

/**
 *  wrapper class with custom logic used for autocompletion 
 *  <br><b>Note:</b> wraps MongoClient partially
 *  <br><br>Reference: <a href="http://php.net/manual/en/class.mongoclient.php">MongoClient</a>
 */
class XMongoClient
{
	/**
	 *  @var static MongoClient object
	 */
	static $connection;
	/**
	 *  @var string url of connected server
	 */
	private $host_address;
	
	/**
	 *  creates a new instance of XMongoClient object
	 *  <br><br>dosen't connect automatically 
	 *  <br><br><b>Note:</b> to establish connection with server call connect()
	 *  @param [$server_address] -> mongodb server address <br>
	 *  @return void
	 */
	function __construct($server_address = "mongodb://127.0.0.1:27017")
	{
		self::$connection = null;
		$this->host_address = $server_address;
		$this->selected_database = null;
		EchoResult::sendResult("<br>CREATED new instance of XMongoClient");
		EchoResult::sendResult("<br> - host address set to " . $server_address);
	}
	
	/**
	 *  free up memory on destroy
	 *  @return void
	 */
	function __destruct()
	{
		// do not disconnect connection on destroy to keep persistent connection
	}
	
	/**
	 *  establishes a connection via tls\ssl with a valid mongodb server upon instantiation
	 *  <br><br><b>Note:</b> terminates script if an error occurs upon connecting to server
	 *  <br><br>Reference: <a href="http://php.net/manual/en/mongoclient.construct.php">MongoClient::__construct</a>
	 *  @param [$username] -> string username of user
	 *  @param [$password] -> string password of user
	 *  @param <b>(optional)</b>[$db_name] -> string name of database authenticating with
	 *  <br> defaults to "admin" authenticating to admin database
	 *  @return void
	 */
	public function connect($username, $password, $db_name = "admin")
	{
		try
		{
			EchoResult::sendResult("<br><br>CONNECTING");
			
			if(!isset(self::$connection))
			{
// 				$ctx = stream_context_create(array(
// 						"ssl" => array(
// 								"cafile" => "/etc/ssl/mongodb.pem",
// 								"local_cert" => "/etc/ssl/blubox-cbs.pem",
// 								"passphrase" => "blubox15",
// 								"allow_self_signed" => false,
// 								"verify_peer" => true,
// 								"verify_peer_name" => true,
// 								"verify_expiry" => true
// 						)
// 				));
				
				$options = array(
// 					"ssl" => true,
					"username" => $username,
					"password" => $password,
					"db" => $db_name,
					"connectTimeoutMS" => 300000,
					"replicaSet" => "cbs0"
				);
				
				self::$connection = new MongoClient($this->host_address, $options);//, array("context" => $ctx));
				if(isset(self::$connection))
				{
					EchoResult::sendResult("<br> - connection with " . self::$connection . " established");
					EchoResult::sendResult("<br> - process id " . self::$connection->getConnections()[0]['hash']);
				}
			}
		}
		catch (MongoConnectionException $e)
		{
			EchoResult::sendResult("<br> - error message: " . $e->getMessage());
			EchoResult::sendResult("<br> - error code: " . $e->getCode());
			die();
		}
	}
	
	/**
	 *  disconnect from connected mongodb server
	 *  @return void
	 */
	public function disconnect()
	{
		try
		{
			EchoResult::sendResult("<br><br> DISCONNECTING");
			if(isset(self::$connection))
			{
				self::$connection->close();
				EchoResult::sendResult("<br> - server connection with " . self::$connection . " closed");
			}
			else 
			{
				EchoResult::sendResult("<br> - there is no connection to disconnect from");
			}
		}
		catch(MongoConnectionException $e)
		{
			EchoResult::sendResult("<br> - error message: " . $e->getMessage());
			EchoResult::sendResult("<br> - error code: " . $e->getCode());
		}
	}
	
	/**
	 *  @return string url of host server on success
	 */
	public function getServerURL()
	{
		if(isset($this->host_address))
		{
			EchoResult::sendResult("<br><br> SERVER URL <br> - " . $this->host_address);
			return $this->host_address;
		}
		else 
		{
			EchoResult::sendResult("<br><br> SERVER URL <br> - url not set");
		}
	}
	
	/**
	 *  display a list of names of all existing database at connected server
	 *  @return void
	 */
	public function listDatabases()
	{
		EchoResult::sendResult("<br><br> LISTING databases at " . self::$connection);
		$num = 0;
		$existing_DBs = self::$connection->listDBs();
		foreach($existing_DBs['databases'] as $dbs)
		{
			EchoResult::sendResult("<br> - " . "index[" . $num . "]" . " = " . $dbs['name']);
			$num++;
		}
	}
	
	/**
	 *  selects an existing database at connected server
	 *  @return XMongoDB object on success | null on fail
	 *  @param [$db_name] -> string name of database
	 */
	public function selectDatabase($db_name)
	{
		EchoResult::sendResult("<br><br> SELECTING " . $db_name . " database in " . self::$connection);
		EchoResult::sendResult("<br> - " . $db_name . " database selected");
		$database = self::$connection->selectDB($db_name);
		if(isset($database))
		{
			return new XMongoDB($database);
		}
		else
		{
			EchoResult::sendResult("<br> - selection failed.. database doesn't exist");
			return null;
		}
	}
	
	/**
	 *  creates a new database in connected server
	 *  @param [$db_name] -> name of database
	 *  @return void
	 */
	public function createDatabase($db_name)
	{
		EchoResult::sendResult("<br><br> CREATING " . $db_name . " database in " . self::$connection);
		$this->selected_database = self::$connection->selectDB($db_name);
		if(isset($this->selected_database))
		{
			EchoResult::sendResult("<br> - creating " . $db_name . " database");
			$dummyCollection = $this->selected_database->createCollection("seed");
			$dummyCollection->drop();
			EchoResult::sendResult("<br> - successfully created " . $db_name . " database");
		}
		else
		{
			EchoResult::sendResult("<br> - failed.. unable to create new database");
		}
	}
	
	/**
	 *  drops an existing database in connected server
	 *  @param [$db_name] -> name of database
	 *  @return void
	 */
	public function dropDatabase($db_name)
	{
		EchoResult::sendResult("<br><br> DROPPING " . $db_name . " database in connected server");
		$database = $this->selectDatabase($db_name);
		if(isset($database))
		{
			$database->drop();
			EchoResult::sendResult("<br> - " . $db_name . " has been dropped");
		}
		else
		{
			EchoResult::sendResult("<br> - failed.. cannot drop database");
		}
	}
	
	/**
	 *  finds out if database with the given name exist in connected server
	 *  @param [$db_name] -> string name of database
	 *  @return boolean <br> true if found <br> false if not found
	 */
	public function databaseExists($db_name)
	{
		EchoResult::sendResult("<br><br> FINDING " . $db_name . " database in connected server");
		$result = false;
		$existingDBs = self::$connection->listDBs();
		foreach ($existingDBs['databases'] as $db)
		{
			if($db_name == $db['name'])
			{
				$result = true;
				break;
			}
		}
		$message = $result ? "<br> - " . $db_name . " database found" :
		"<br> - " . $db_name . " database not found";
		EchoResult::sendResult($message);
		return $result;
	}
}