<?php

require_once dirname(__DIR__) . '/Debugging/EchoResult.php';

/**
 *  wrapper class with custom logic used for autocompletion
 *  <br><b>Note:</b> wraps MongoCollection partially
 *  <br><br>Reference: <a href="http://php.net/manual/en/class.mongocollection.php">MongoCollection</a>
 */
class XMongoCollection
{
	/**
	 *  @var MongoCollection object
	 */
	private $collection;
	/**
	 *  @var string name of MongoCollection object
	 */
	private $collection_name;
	
	/**
	 *  creates a new instance of XMongoCollection object
	 *  @param [$mongo_collection] -> mongo collection object <br>
	 *  @return void
	 */
	function __construct($mongo_collection)
	{
		$this->collection = $mongo_collection;
		$this->collection_name = $this->collection->getName();
		EchoResult::sendResult("<br><br>CREATED new instance of XMongoCollection");
		EchoResult::sendResult("<br> - collection set to " . $this->collection_name);
	}
	
	/**
	 *  free up memory on destroy
	 *  @return void
	 */
	function __destruct()
	{
		unset($GLOBALS['collection']);
		unset($GLOBALS['collection_name']);
	}

	/**
	 *  displays information on documents in MongoCollection object
	 *  @return void
	 */
	public function displayCollectionInfo()
	{
		EchoResult::sendResult("<br><br> DISPLAYING documents information in " . $this->collection_name);
		$info = iterator_to_array($this->collection->find());
		$count = 0;
		foreach($info as $key => $value)
		{
			EchoResult::sendResult("<br><br> element[" . $count . "] <br>");
			foreach ($value as $a => $b)
			{
				EchoResult::sendResult("key[" . $a . "] => " . $b . "<br>");
			}
			$count++;
		}
	}
	
	/**
	 *  @return string name of MongoCollection object
	 */
	public function getName()
	{
		EchoResult::sendResult("<br><br> NAME of selected collection <br> - " . $this->collection_name);
		return $this->collection_name;
	}	
	
	/**
	 *  @return integer number of documents in MongoCollection object
	 */
	public function getNumDocuments()
	{
		EchoResult::sendResult("<br><br> COUNTING documents in " . $this->collection_name);
		$count = $this->collection->count();
		EchoResult::sendResult("<br> - " . $count . " documents exist");
		return $count;
	}

	/**
	 *  finds an existing record of a document based on unique id
	 *  @return boolean <br> ChatUser object on success <br> null on fail
	 *  @param [$unique_ID] -> unique MongoID string value <br>
	 *  References: <a href="http://php.net/manual/en/mongocollection.find.php">MongoCollection::find</a>
	 */
	public function findDocumentByID($unique_ID)
	{
		EchoResult::sendResult("<br><br> FINDING ONE USER with unique id " . '["' . $unique_ID . '"]' . " in " . $this->collection->getName());
		$query = array(ReferenceKeys::CHAT_USER_ID => new MongoID($unique_ID));
		$result = iterator_to_array($this->collection->find($query));
		if(count($result)>0)
		{
			EchoResult::sendResult("<br> - document found");
			$chat_user = new ChatUser();
			foreach ($result as $element => $arr)
			{
				foreach ($arr as $key => $value)
				{
					$chat_user->addCredential($key, $value);
				}
			}
			return $chat_user;
		}
		else
		{
			EchoResult::sendResult("<br> - document doesn't exist");
			return null;
		}
	}
	
	/**
	 *  finds an existing record of a document based on unique username
	 *  @return ChatUser object on success <br> null on fail
	 *  @param [$username] -> unique string name of user <br>
	 *  References: <a href="http://php.net/manual/en/mongocollection.find.php">MongoCollection::find</a>
	 */
	public function findDocumentByName($username)
	{
		EchoResult::sendResult("<br><br> FINDING ONE USER with unique name " . '["' . $username . '"]' . " in " . $this->collection_name);
		$query = $this->collection->getName() == "system.users" ? 
			array(ReferenceKeys::CHAT_USER_ADMIN_USERNAME => $username) : 
			array(ReferenceKeys::CHAT_USER_USERNAME => $username);
		
		$result = iterator_to_array($this->collection->find($query));
		if(count($result)>0)
		{
			EchoResult::sendResult("<br> - document exist");
			$chat_user = new ChatUser();
			foreach ($result as $element => $arr)
			{
				foreach ($arr as $key => $value)
				{
					$chat_user->addCredential($key, $value);
				}
			}
			return $chat_user;
		}
		else
		{
			EchoResult::sendResult("<br> - document doesn't exist");
			return null;
		}
	}
	
	/**
	 *  finds all documents that matches the supplied query 
	 *  @return cursor to array of matching documents to the query
	 *  @param [$query] -> array of key value pairs
	 */
	public function find($query)
	{
		$result = iterator_to_array($this->collection->find($query));
		return $result;
	}
	
	/**
	 *  insert document into MongoDB collection
	 *  @param [$document] -> associative array object <br>
	 *  References: <a href="http://php.net/manual/en/mongocollection.insert.php">MongoCollection::insert</a>
	 *  @return void
	 */
	public function insertDocument($document)
	{
		EchoResult::sendResult("<br><br> INSERTING document to " . $this->collection_name);
		if($this->collection->insert($document))
		{
			EchoResult::sendResult("<br> - document successfully inserted");
		}
		else
		{
			EchoResult::sendResult("<br> - document insert failed");
		}
	}
	
	/**
	 *  insert document based on unique username
	 *  <br><br><b>Note:</b> use this if unique id and username is the only thing needed to be tracked
	 *  <br>use insertDocument() otherwise
	 *  @param [$username] -> string name of document
	 *  @param <b>(optional)</b>[$id] -> unique id of document
	 *  <br><b>Note:</b> if id is left blank a new MongoId is automatically generated
	 *  @return void
	 */
	public function insertDocumentByName($username)
	{
		EchoResult::sendResult("<br><br> INSERTING document to " . $this->collection_name);
		$document = array ( ReferenceKeys::CHAT_USER_USERNAME => $username );
		if($this->collection->insert($document))
		{
			EchoResult::sendResult("<br> - document successfully inserted");
		}
		else
		{
			EchoResult::sendResult("<br> - document insert failed");
		}
	}
	
	/**
	 *  finds an existing record of a document based on unique id <br>
	 *  updates record of document with existing id based on key with new value given
	 *  @return void
	 *  @param[$unique_ID] -> unique MongoID string value <br>
	 *  @param[$key] -> existing key in collection being updated <br>
	 *  @param[$new_value] -> new value of record with given key <br>
	 *  References: <a href="http://php.net/manual/en/mongocollection.update.php">MongoCollection::update</a>
	 */
	public function updateDocumenByID($unique_ID, $key, $new_value)
	{
		EchoResult::sendResult("<br><br> UPDATING " . '["' . $key . '"]' . " of user with unique id " . '["' . $unique_ID . '"]');
		$findUserQuery = array(ReferenceKeys::CHAT_USER_ID => new MongoID($unique_ID));
		$updateQuery = array(UpdateOperators::SET => array( $key => $new_value ));
		if($this->collection->update($findUserQuery, $updateQuery))
		{
			EchoResult::sendResult("<br> - " . $key . " successfully updated to " . $new_value);
		}
		else
		{
			EchoResult::sendResult("<br> - " . $key . " update failed");
		}
	}
	
	/**
	 *  finds an existing record of a document based on unique name of user <br>
	 *  updates record of document with existing username based on key with new value given
	 *  @return void
	 *  @param[$username] -> unique string name of user <br>
	 *  @param[$key] -> existing key in collection being updated <br>
	 *  @param[$new_value] -> new value of record with given key <br>
	 *  References: <a href="http://php.net/manual/en/mongocollection.update.php">MongoCollection::update</a>
	 */
	public function updateDocumentByName($username, $key, $new_value)
	{
		EchoResult::sendResult("<br><br> UPDATING " . '["' . $key . '"]' . " of user with unique id " . '["' . $username . '"]');
		$findUserQuery = array(ReferenceKeys::CHAT_USER_USERNAME => $username);
		$updateQuery = array(UpdateOperators::SET => array( $key => $new_value ));
		if($this->collection->update($findUserQuery, $updateQuery))
		{
			EchoResult::sendResult("<br> - " . $key . " successfully updated to " . $new_value);
		}
		else
		{
			EchoResult::sendResult("<br> - " . $key . " update failed");
		}
	}
	
	/**
	 *  handles removing of documents from currently selected collection based on unique id
	 *  @return void
	 *  @param [$unique_ID] -> unique MongoID string value <br>
	 *  References: <a href="http://php.net/manual/en/mongocollection.remove.php">MongoCollection::remove</a>
	 */
	public function removeDocumentByID($unique_ID)
	{
		EchoResult::sendResult("<br><br> REMOVING record in " . $this->collection_name);
		$query = array(ReferenceKeys::CHAT_USER_ID => new MongoID($unique_ID));
		if($this->collection->remove($query))
		{
			EchoResult::sendResult("<br> - document with id " . $unique_ID . " has been removed from " . $this->collection_name);
			EchoResult::sendResult("<br> - document removed in " . $this->collection_name);
		}
		else 
		{
			EchoResult::sendResult("<br> - document removal failed..");
		}
	}
	
	/** handles removing of documents from currently selected collection based on unique name of user
	 *  @return void
	 *  @param [$username] -> unique string name of user <br>
	 *  References: <a href="http://php.net/manual/en/mongocollection.remove.php">MongoCollection::remove</a>
	 */
	public function removeDocumentByName($username)
	{
		EchoResult::sendResult("<br><br> REMOVING record in " . $this->collection_name);
		$query = array(ReferenceKeys::CHAT_USER_USERNAME => $username);
		if($this->collection->remove($query))
		{
			EchoResult::sendResult("<br> - document with name " . $username . " has been removed from " . $this->collection_name);
			EchoResult::sendResult("<br> - document removed in " . $this->collection_name);
		}
		else 
		{
			EchoResult::sendResult("<br> - document removal failed..");
		}
	}
}