<?php

require_once dirname(__DIR__) . '/Debugging/EchoResult.php';
require_once dirname(__DIR__) . '/Constants/DBAccess.php';

/**
 *  wrapper class with custom logic used for autocompletion
 *  <br><b>Note:</b> wraps MongoCollection partially
 *  <br><br>Reference: <a href="http://php.net/manual/en/class.mongodb.php">MongoDB</a>
 */
class XMongoDB 
{
	/**
	 *  @var MongoDB object
	 */
	private $database;
	/**
	 *  @var string name of MongoDB object
	 */
	private $database_name;
	
	/**
	 *  creates a new instance of XMongoDB object
	 *  @param [$mongo_db] -> mongodb object <br>
	 *  @return void
	 */
	function __construct($mongo_db)
	{
		$this->database = $mongo_db;
		$this->database_name = $this->database->__toString();
		EchoResult::sendResult("<br><br>CREATED new instance of XMongoDB");
		EchoResult::sendResult("<br> - database set to " . $this->database_name);
	}
	
	/**
	 *  free up memory on destroy
	 *  @return void
	 */
	function __destruct()
	{
		unset($GLOBALS['database']); 
		unset($GLOBALS['database_name']);
	}
	
	/**
	 *  adds new user credentials for database access via executing a database command
	 *  <br><br><b>Note:</b> default role is readWriteAnyDatabase in admin database
	 *  <br><br>References: <a href="http://php.net/manual/en/mongodb.command.php">MongoDB::command</a>
	 *  @param [$username] -> username of new user
	 *  @param <b>(optional)</b>[$password] -> password of new user
	 *  @return void
	 */
	public function addUser($username, $password = DBAccess::USER_DEFAULT_PASS)
	{
		$command = array(
						"createUser" => $username,
						"pwd" => $password,
						"roles" => array( 
									array(
										"role" => "readWriteAnyDatabase",
										"db" => "admin"
										)
									)
						);
		$this->database->command($command);
	}
	
	/**
	 *  @return string name of MongoDB object
	 */
	public function getName()
	{
		EchoResult::sendResult("<br><br> NAME of selected database <br> - " . $this->database_name);
		return $this->database_name;
	}
	
	/**
	 *  @return integer number of collections in selected database
	 */
	public function getNumCollections()
	{
		EchoResult::sendResult("<br><br> COUNTING number of collections in " . $this->database_name);
		$count = 0;
		foreach($this->database->listCollections() as $collection_name)
		{
			$count++;
		}
		EchoResult::sendResult("<br> - " . $count . " collections exist");
		return $count;
	}
	
	/**
	 *  returns an array of XMongoCollection objects<br>
	 *  <br>References: <a href="http://php.net/manual/en/mongodb.listcollections.php">MongoDB::listCollection</a>
	 *  @return XMongoCollection array
	 */
	public function getCollections()
	{
		$result = array();
		foreach($this->database->listCollections() as $collection)
		{
			array_push($result, new XMongoCollection($collection));
		}
		return $result;
	}
	
	/**
	 *  display a list of names of all existing collections in MongoDB object
	 *  <br><br> <b>Note:</b> set debugEcho to true to display output
	 *  @return void
	 */
	public function listCollections()
	{
		EchoResult::sendResult("<br><br> LISTING collections in " . $this->database_name);
		$count = 0;
		foreach($this->database->listCollections() as $collection_name)
		{
			EchoResult::sendResult("<br> - " . $collection_name->getName());
			$count++;
		}
		if($count == 0) { EchoResult::sendResult("<br> - 0 collections exist in " . $this->database_name); }
	}

	/**
	 *  selects an existing collection in MongoDB object
	 *  @return XMongoCollection object on success | null on fail
	 *  @param [$collection_name] -> string name of collection
	 */
	public function selectCollection($collection_name)
	{
		EchoResult::sendResult("<br><br> SELECTING " . $collection_name . " collection in selected database");
		$collection = $this->database->selectCollection($collection_name);
		if(isset($collection))
		{
			EchoResult::sendResult("<br> - " . $collection_name . " collection selected");
			return new XMongoCollection($collection);
		}
		else
		{
			EchoResult::sendResult("<br> - selection failed.. collection doesn't exist");
			return null;
		}
	}
	
    /**
	 *  creates a new collection in MongoDB object
	 *  @param [$name] -> string name of database
	 *  @param <b>(optional)</b>[$capped] -> if the collection should be fixed size
	 *  @param <b>(optional)</b>[$max] -> maximum number of elements to store in the collection if fixed size 
	 *  @param <b>(optional)</b>[$auto_index_id] -> if collection adds index id automatically
	 *  <br>References: <a href="http://php.net/manual/en/mongocollection.createcollection.php">MongoCollection::createCollection</a>
	 *  @return void
	 */
	public function createCollection($name, $auto_index_id = true, $capped = false, $max = 999)
	{
		EchoResult::sendResult("<br><br> ADDING " . $name . " collection to " . $this->database_name);
		$options = array ( "capped" => $capped, "max" => $max,"autoIndexID" => $auto_index_id );
		$collection = $this->database->createCollection($name, $options);
		if(isset($collection))
		{
			EchoResult::sendResult("<br> - " . $name . " collection added");
		}
		else 
		{
			EchoResult::sendResult("<br> - failed.. couldn't create new collection");
		}
	}
	
	/**
	 *  drops an existing collection in selected database
	 *  @param [$collection_name] -> name of collection
	 *  @return void
	 */
	public function dropCollection($collection_name)
	{
		EchoResult::sendResult("<br><br> DROPPING " . $collection_name . " in " . $this->database_name);
		$collection = $this->selectCollection($collection_name);
		if(isset($collection))
		{
			$collection->drop();
			EchoResult::sendResult("<br> - " . $collection_name . " has been dropped");
		}
		else
		{
			EchoResult::sendResult("<br> - collection drop failed.. no collection selected");
		}
	}
	
	/**
	 *  finds out if collection with the given name exist in MongoDB object
	 *  <br><br><b>Note:</b> includes system collections
	 *  @param [$collection_name] -> string name of collection
	 *  @return boolean <br> true if found <br> false if not found
	 */
	public function collectionExists($collection_name)
	{
		EchoResult::sendResult("<br><br> FINDING " . $collection_name . " collection in " . $this->database_name);
		$result = false;
		foreach ($this->database->getCollectionNames(array("includeSystemCollections" => true)) as $db_collection_name)
		{
			if($collection_name == $db_collection_name)
			{
				$result = true;
				break;
			}
		}
		$message = $result ? "<br> - " . $collection_name . " collection found" :
		"<br> - " . $collection_name . " collection not found";
		EchoResult::sendResult($message);
		return $result;
	}
}