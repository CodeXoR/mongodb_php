<?php

/** handles auto loading of file classes */
require_once __DIR__ . "/Autoloader/autoloader.php";

/**
 *  SCRIPT FUNCTIONS
 */
function createChatRoom($machine, $machine_name, $room_number)
{
	$new_room_name = $machine_name . " 00" . strval($room_number);
	$machine->createCollection($new_room_name);
	return $machine->selectCollection($new_room_name);
}

function connectToChatRoom($username, $user, $room, $users)
{
	if(is_null($user->getUserCredential(ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED)))
	{
		$room->insertDocumentByName($username);
		$users->updateDocumentByName($username, ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED, $room->getName());
		$timeStamp = new DateTime("NOW", new DateTimeZone(TimeStampSettings::DATE_TIMEZONE));
		$users->updateDocumentByName($username, ReferenceKeys::CHAT_USER_TIMESTAMP, $timeStamp->format(DateTime::W3C));
	}
	
	$num_chat_users = $room->getNumDocuments();

	$chat_room_info = array(
		"roomName" => $room->getName(),
		"maxPlayers" => ChatRoomSettings::MAX_USER_PER_ROOM,
		"numPlayers" => $num_chat_users
	);

	// send a response to the client giving the name of the chat room where chat user just entered
	EchoResult::sendResultOnce(json_encode(array("chatRoomInfo" => $chat_room_info)));
}

/**
 *  DEBUGGING MODE
 */
// turn this off for production test
// turn this on only for debugging
EchoResult::$result_mode = EchoResultMode::OFF;

/**
 *  INFORMATION SENT FROM CLIENT
 */
// supplied username of client
$chat_user_name = $_POST[ReferenceKeys::CHAT_USER_USERNAME];

// get supplied machine name from client
$machine_db_name = $_POST[ReferenceKeys::DB_MACHINE];

/**
 *  SERVER CONNECTION AS ADMINISTRATOR
 */
// connect to server
$chat_server = new XMongoClient(ServerHostAddress::AWS_HOST);
$chat_server->connect(DBAccess::SUPERUSER_ADMIN_USERNAME, DBAccess::SUPERUSER_ADMIN_PASSWORD);

/**
 *  VERIFICATION OF USER CREDENTIALS BY ADMINISTRATOR
 */
// get reference to chat users database
$chat_users_database = $chat_server->selectDatabase(ReferenceKeys::DB_CHAT);

// get reference to chat users collection
$chat_users = $chat_users_database->selectCollection(ReferenceKeys::COLLECTION_CHAT_USERS);

// check if user exist in chat users database
$chat_user = $chat_users->findDocumentByName($chat_user_name);

// if user credentials are invalid terminate this script
if(is_null($chat_user))
{
	// send an ambiguous failure notice
	EchoResult::sendResultOnce("failed to connect..");
	return;
}

/**
 *  MACHINE DATABASE LOOKUP AND CREATION IF NECESSARY
 */
// check if machine exists in the server
// create a new machine with the supplied name if machine is not found in the server
if(!$chat_server->databaseExists($machine_db_name))
{
	$chat_server->createDatabase($machine_db_name);
}

// select chat database with specified machine name
$machine_database = $chat_server->selectDatabase($machine_db_name);

/**
 *  MACHINE DATABASE CHATROOM COLLECTION LOOKUP AND CREATION IF NECESSARY
 *  CHAT USER CHAT ROOM INCLUSION 
 */
// check for existing chat rooms in machine
$num_chat_rooms = $machine_database->getNumCollections();

// create a new room if there are no rooms available
// or create a new room if all the rooms have reached their maximum capacity
// add chat user to the first open room found in machine 
if($num_chat_rooms==0)
{
	$chat_room = createChatRoom($machine_database, $machine_db_name, $num_chat_rooms+1);
	connectToChatRoom($chat_user_name, $chat_user, $chat_room, $chat_users);
}
// chat rooms exist
else
{
	// flag to check if user has been successfully added to an existing open room
	$flag = false;
	// iterate through all existing rooms
	foreach($machine_database->getCollections() as $chat_room)
	{	
		// check for number of users in room
		// each room has maximum capacity   
		// add user to room
		if($chat_room->getNumDocuments() < ChatRoomSettings::MAX_USER_PER_ROOM)
		{
			connectToChatRoom($chat_user_name, $chat_user, $chat_room, $chat_users);
			$flag = true;
			break;
		}
	}
	// at this point all existing rooms are at maximum capacity
	// create a new room and add user to that room
	if($flag==false)
	{
		$chat_room = createChatRoom($machine_database, $machine_db_name, $num_chat_rooms+1);
		connectToChatRoom($chat_user_name, $chat_user, $chat_room, $chat_users);
	}
}