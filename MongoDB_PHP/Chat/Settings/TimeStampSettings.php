<?php

/**
 *  constant string variables for timeStamp formatting
 *  <br><br><b>Note:</b> change values here if needed
 */
class TimeStampSettings
{
	/**
	 *  @var const string variable timezone area
	 */
	const DATE_TIMEZONE = "America/Los_Angeles";
}

?>