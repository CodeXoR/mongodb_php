<?php 

/**
 *  defined constants for setting up chat rooms
 *  <br><br><b>Note:</b> change values here if needed
 */
class ChatRoomSettings
{
	/**
	 *  @var constant variable for tracking maximum number of people able to join a single chat room
	 */
	const MAX_USER_PER_ROOM = 30;
}

?>