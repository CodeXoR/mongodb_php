<?php

/**
 *  defined constants for chat user account creation
 *  <br><br><b>Note:</b> change values here if needed
 */
class ChatUserStartValues
{
	/**
	 *  @var constant variable string starting xp of new chat user
	 */
	const START_XP = "1.1";
	
	/**
	 *  @var constant variable integer starting balance of new chat user
	 */
	const START_BALANCE = 999;
}

?>