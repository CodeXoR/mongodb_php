<?php 

/** handles auto loading of file classes */
require_once __DIR__ . "/Autoloader/autoloader.php";

/**
 *  DEBUGGING MODE
 */
// turn this off for production test
// turn this on only for debugging
EchoResult::$result_mode = EchoResultMode::OFF;

/**
 *  INFORMATION SENT FROM CLIENT
 */
// supplied username of client
$chat_user_name = $_POST[ReferenceKeys::CHAT_USER_USERNAME];

/**
 *  SERVER CONNECTION AS ADMINISTRATOR
 */
// create a connection with server
$chat_server = new XMongoClient(ServerHostAddress::AWS_HOST);
$chat_server->connect(DBAccess::SUPERUSER_ADMIN_USERNAME, DBAccess::SUPERUSER_ADMIN_PASSWORD);

/**
 *  ACCOUNT INFORMATION RETRIEVAL
 */
// select admin database
$chat_users_db = $chat_server->selectDatabase(ReferenceKeys::DB_CHAT);

// select users collection
$chat_users = $chat_users_db->selectCollection(ReferenceKeys::COLLECTION_CHAT_USERS);

// get chat user info
$chat_user = $chat_users->findDocumentByName($chat_user_name);

// if chat user credentials is not valid
if(is_null($chat_user))
{
	// send ambiguous failure notice
	EchoResult::sendResultOnce("failed to connect..");
}
// if chat user credentials is valid
else 
{
	// send chat user info to client as a json document
	// not including _id, username, and chatRoomJoined key value pairs to the response
	EchoResult::sendResultOnce(json_encode(array("userInfo" => $chat_user->getUserCredentials())));
}

?>