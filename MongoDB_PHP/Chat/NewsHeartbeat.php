<?php

/** handles auto loading of file classes */
require_once __DIR__ . "/Autoloader/autoloader.php";

// turn this off for production test
// turn this on only for debugging
EchoResult::$result_mode = EchoResultMode::OFF;

$news1 = "News and Deals Information will go here..";
$news2 = "Fantasy Dragons Slots on free rotation";
$news3 = "Coins at 20% off until October 28, 2028";

$news_feed = array ( $news1, $news2, $news3 );

EchoResult::sendResultOnce(json_encode(array("newsFeed" => $news_feed[mt_rand(0, count($news_feed)-1)])));

?>