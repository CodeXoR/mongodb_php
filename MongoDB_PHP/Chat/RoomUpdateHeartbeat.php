<?php

/** handles auto loading of file classes */
require_once __DIR__ . "/Autoloader/autoloader.php";

/**
 *  DEBUGGING MODE
 */
// turn this off for production test
// turn this on only for debugging
EchoResult::$result_mode = EchoResultMode::OFF;

// get supplied machine name from client
$chat_user_name = $_POST[ReferenceKeys::CHAT_USER_USERNAME];

$machine_db_name = $_POST[ReferenceKeys::DB_MACHINE];

/**
 *  SERVER CONNECTION AS ADMINISTRATOR
 */
// create a connection with server
$chat_server = new XMongoClient(ServerHostAddress::AWS_HOST);
$chat_server->connect(DBAccess::SUPERUSER_ADMIN_USERNAME, DBAccess::SUPERUSER_ADMIN_PASSWORD);

/**
 *  VERIFICATION OF USER CREDENTIALS BY ADMINISTRATOR
 */
// get reference to chat users database
$chat_users_database = $chat_server->selectDatabase(ReferenceKeys::DB_CHAT);

// get reference to chat users collection
$chat_users = $chat_users_database->selectCollection(ReferenceKeys::COLLECTION_CHAT_USERS);

// check if user exist in chat users database
$chat_user = $chat_users->findDocumentByName($chat_user_name);

// if user credentials are invalid terminate this script
if(is_null($chat_user))
{
	// send an ambiguous failure notice
	EchoResult::sendResultOnce("failed to connect..");
	return;
}

/**
 *  HEARTBEAT PACKAGE CREATION AND SENDING
 */
// select chat database with specified machine name
$machine_database = $chat_server->selectDatabase($machine_db_name);

// select chat room collection from chat room database
$chat_room = $machine_database->selectCollection($chat_user->getUserCredential(ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED));

// current number of chat users in selected chat room 
$num_chat_users = $chat_room->getNumDocuments();

$chat_room_info = array(
	"maxPlayers" => ChatRoomSettings::MAX_USER_PER_ROOM,
	"numPlayers" => $num_chat_users
);

EchoResult::sendResultOnce(json_encode(array("chatRoomInfo" => $chat_room_info))); 

?>