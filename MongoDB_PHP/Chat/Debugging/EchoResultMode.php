<?php 

/**
 *  made up enum for turning custom debugger on/off
 */
class EchoResultMode
{
	/**
	 *  @var const boolean
	 */
	const ON = true;
	/**
	 *  @var const boolean
	 */
	const OFF = false;
	/**
	 *  @var const boolean default is OFF
	 */
	const __default = self::OFF;
}
	
?>