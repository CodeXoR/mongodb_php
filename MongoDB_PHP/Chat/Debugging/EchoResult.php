<?php 

require_once 'EchoResultMode.php';

/**
 *  utility class used for passing results to client through echo method
 */
class EchoResult
{
	/**
	 *  @var static boolean variable 
	 */
	static $result_mode = EchoResultMode::OFF;
	
	/**
	 *  echo the result if result mode is turned on
	 *  <br><br><b>Note:</b> use this function for debugging | works only on EchoResultMode::ON
	 *  @return void
	 *  @param [$result] -> result variable most often strings and numbers 
	 */
	public static function sendResult($result)
	{
		if(self::$result_mode == EchoResultMode::ON)
		{
			echo $result;
		}
	}
	
	/**
	 *  echo the result if result mode is turned off
	 *  <br><br><b>Note:</b> use this function if needed to echo a result independent from any debug results | works only on EchoResultMode::OFF
	 *  @return void
	 *  @param [$result] -> result variable most often strings and numbers 
	 */
	public static function sendResultOnce($result)
	{
		if(self::$result_mode == EchoResultMode::OFF)
		{
			echo $result;
		}
	}
}

?>