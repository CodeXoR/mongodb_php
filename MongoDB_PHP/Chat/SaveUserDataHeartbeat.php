<?php

/** handles auto loading of file classes */
require_once __DIR__ . "/Autoloader/autoloader.php";

/**
 *  DEBUGGING MODE
 */
// turn this off for production test
// turn this on only for debugging
EchoResult::$result_mode = EchoResultMode::OFF;

/**
 *  INFORMATION SENT FROM CLIENT
 */
// supplied username of client
$chat_user_name = $_POST[ReferenceKeys::CHAT_USER_USERNAME];

// current xp of user at client
$chat_user_xp = $_POST[ReferenceKeys::CHAT_USER_XP];

// current balance of user at client
$chat_user_balance = intval($_POST[ReferenceKeys::CHAT_USER_BALANCE]);

/**
 *  SERVER CONNECTION AS ADMINISTRATOR
 */
// connect to server
$chat_server = new XMongoClient(ServerHostAddress::AWS_HOST);
$chat_server->connect(DBAccess::SUPERUSER_ADMIN_USERNAME, DBAccess::SUPERUSER_ADMIN_PASSWORD);

/**
 *  ACCOUNT INFORMATION UPDATE
 */
// get reference to chat users database
$chat_users_database = $chat_server->selectDatabase(ReferenceKeys::DB_CHAT);

// get reference to chat users collection
$chat_users = $chat_users_database->selectCollection(ReferenceKeys::COLLECTION_CHAT_USERS);

// get reference of chat user with supplied username from client in chat database
$chat_user = $chat_users->findDocumentByName($chat_user_name);

if(is_null($chat_user))
{
	// send an ambiguous failure reason
	EchoResult::sendResultOnce("failed to update..");
}
else 
{
	$chat_users->updateDocumentByName($chat_user_name, ReferenceKeys::CHAT_USER_XP, $chat_user_xp);
	$chat_users->updateDocumentByName($chat_user_name, ReferenceKeys::CHAT_USER_BALANCE, $chat_user_balance);
	$timeStamp = new DateTime("NOW", new DateTimeZone(TimeStampSettings::DATE_TIMEZONE));
	$chat_users->updateDocumentByName($chat_user_name, ReferenceKeys::CHAT_USER_TIMESTAMP, $timeStamp->format(DateTime::W3C));
	EchoResult::sendResultOnce("player info of " . $chat_user_name . " updated");
}

?>