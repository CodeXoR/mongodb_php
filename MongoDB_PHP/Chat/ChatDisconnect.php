<?php

/** handles auto loading of file classes */
require_once __DIR__ . "/Autoloader/autoloader.php";

/**
 *  DEBUGGING MODE
 */
// turn this off for production test
// turn this on only for debugging
EchoResult::$result_mode = EchoResultMode::OFF;

/**
 *  INFORMATION SENT FROM CLIENT
 */
// supplied username of client
$chat_user_name = $_POST[ReferenceKeys::CHAT_USER_USERNAME];

// get supplied machine name from client
$machine_db_name = $_POST[ReferenceKeys::DB_MACHINE];

/**
 *  SERVER CONNECTION AS ADMINISTRATOR
 */
// connect to server
$chat_server = new XMongoClient(ServerHostAddress::AWS_HOST);
$chat_server->connect(DBAccess::SUPERUSER_ADMIN_USERNAME, DBAccess::SUPERUSER_ADMIN_PASSWORD);

/**
 *  VERIFICATION OF USER CREDENTIALS BY ADMINISTRATOR
 */
// get reference to chat users database
$chat_users_database = $chat_server->selectDatabase(ReferenceKeys::DB_CHAT);

// get reference to chat users collection
$chat_users = $chat_users_database->selectCollection(ReferenceKeys::COLLECTION_CHAT_USERS);

// check if user exist in chat users database
$chat_user = $chat_users->findDocumentByName($chat_user_name);

// if user credentials are invalid terminate this script
if(is_null($chat_user))
{
	// send an ambiguous failure notice
	EchoResult::sendResultOnce("failed to connect..");
	return;
}

/**
 *  VERIFICATION IF USER IS CURRENTLY IN A CHAT ROOM
 *  USER DISCONNECTION
 */
// select chat database with specified machine name
$machine_database = $chat_server->selectDatabase($machine_db_name);

// terminate this script if database is not found
if(is_null($machine_database))
{
	EchoResult::sendResultOnce("database with name " . $machine_db_name . " doesn't exist");
	return;
}

// select chat room collection with specified name
$chat_room = $machine_database->selectCollection($chat_user->getUserCredential(ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED));

// terminate this script if collection is not found
if(is_null($chat_room))
{
	EchoResult::sendResultOnce("collection with name " . $chat_room_name . " doesn't exist");
	return;
}

// remove user from chat room
$chat_room->removeDocumentByName($chat_user_name);
EchoResult::sendResultOnce("user " . $chat_user_name . " left " . $chat_user->getUserCredential(ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED));
$chat_users->updateDocumentByName($chat_user_name, ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED, null);

?>