<?php

/**
 *  class holding information about payouts associated with a specific chat bonus
 */
class BonusPayout
{
	/**
	 *  @var integer amount of coins given to the chat user if given the bonus
	 */
	private $coin_payout;
	/**
	 *  @var string amount of xp given to user if given the bonus
	 */
	private $xp_payout;
	
	/**
	 *  creates a new instance of BonusPayout
	 *  @param [$coin] -> integer amount of coins given to the chat user if given the bonus
	 *  @param [$xp] -> float amount of xp given to user if given the bonus
	 */
	function __construct($coin, $xp)
	{
		$this->coin_payout = $coin;
		$this->xp_payout = $xp;
	}
	
	/**
	 *  free up memory on destroy
	 */
	function __destruct()
	{
		unset($GLOBALS['coin_payout']);
		unset($GLOBALS['xp_payout']);
	}
	
	/**
	 *  @return associative array ready for json encoding <br> consists of all information about bonus payout
	 */
	public function getPayoutInfo()
	{
		$info = array(
				"coinPayout" => $this->coin_payout,
				"xpPayout" => $this->xp_payout
		);
		return $info;
	}
}

?>