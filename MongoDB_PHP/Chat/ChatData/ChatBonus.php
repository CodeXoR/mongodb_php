<?php

/**
 *  class holding all information about a heartbeat chat bonus
 */
class ChatBonus
{
	/**
	 *  @var string name tag of chat bonus
	 */
	private $name;
	/**
	 *  @var string description of chat bonus
	 */
	private $description;
	/**
	 *  @var string array of possible words sent by client in the chat room 
	 */
	private $potential_values;
	/**
	 *  @var BonusPayout object 
	 */
	private $payout_value;
	
	/**
	 *  creates a new instance of ChatBonus
	 *  @param [$name] -> string name of chat bonus
	 *  @param [$description] -> string description of chat bonus
	 *  @param [$potential_values] -> string array of possible words sent by client in the chat room 
	 *  @param [$payout_coin] -> integer amount of coin given to chat user if given the bonus
	 *  @param [$payout_xp] -> float amount of xp given to user if given the bonus
	 *  @return void
	 */
	function __construct($name, $description, $potential_values, $payout_coin, $payout_xp)
	{
		$this->name = $name;
		$this->description = $description;
		$this->potential_values = $potential_values;
		$this->payout_value = new BonusPayout($payout_coin, $payout_xp);
	}
	
	/**
	 *  free up memory on destroy
	 */
	function __destruct()
	{
		unset($GLOBALS['name']);
		unset($GLOBALS['description']);
		unset($GLOBALS['potential_values']);
		unset($GLOBALS['payout_value']);
	}
	
	/**
	 *  @return associative array ready for json encoding <br> consists of all information about chat bonus
	 */
	public function getInfo()
	{
		$info = array(
				"name" => $this->name,
				"description" => $this->description,
				"potentialValues" => $this->potential_values,
				"payoutValue" => $this->payout_value->getPayoutInfo()
		);
		return $info;
	}
}

?>