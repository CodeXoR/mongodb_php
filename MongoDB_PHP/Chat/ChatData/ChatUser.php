<?php 

class ChatUser
{
	/**
	 *  @var associative array of key value pairs
	 */
	private $credentials;
	
	/**
	 *  creates a new instance of ChatUser
	 *  @return void
	 */
	function __construct()
	{
		$this->credentials = array();
	}
	
	/**
	 *  free up memory on destroy
	 *  @return void
	 */
	function __destruct()
	{
		if(isset($this->credentials)) { unset($GLOBALS['credentials']); }
	}
	
	public function addCredential($key, $value)
	{
		$this->credentials[$key] = $value;
	}
	
	/**
	 *  @return associative array consisting of key value pairs for this document only includes xp, balance, and last timeStamp
	 */
	public function getUserCredentials()
	{
		$credentials = array(
			ReferenceKeys::CHAT_USER_XP => $this->credentials[ReferenceKeys::CHAT_USER_XP],
			ReferenceKeys::CHAT_USER_BALANCE => $this->credentials[ReferenceKeys::CHAT_USER_BALANCE],
			ReferenceKeys::CHAT_USER_TIMESTAMP => $this->credentials[ReferenceKeys::CHAT_USER_TIMESTAMP]
		);
		return $credentials;
	}
	
	/**
	 *  @param [$key] -> specific document key to check value for
	 *  @return value associated with the given key on success
	 *  <br> null on fail
	 */
	public function getUserCredential($key)
	{
		$data = $this->credentials[$key];
		if(isset($data))
		{
			EchoResult::sendResult("<br><br> successfully retrieved credentials");
		}
		else 
		{
			EchoResult::sendResult("<br><br> failed to retrieve credentials");
		}
		return $data;
	}
	
	/**
	 *  @return associative array consisting of all key value pairs for this document
	 */
	public function getFullUserCredentials()
	{
		return $this->credentials;
	}
}

?>