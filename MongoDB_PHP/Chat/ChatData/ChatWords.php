<?php

/**
 *  lookup table of possible words sent by chat users
 */
class ChatWords
{
	/**
	 *  @var static string array of possible one worders
	 */
	static $one_worders = [
			"Poop", "Yeah", "Blah", "Boo", "Fuuuuuuuu", "Shiiiiiiii", "Ughhh", "Jeeezzz", "Sup", "Dawg", "Huhu", "Hehe",
			"Dandanan", "Dude", "Where's", "my", "Car", "dooodooo", "Bababa", "Babanana", "Noooooooooo", "Yesssssss", "soso", 
			"lalalalala", "I", "am", "hungry", "but", "gotta", "diet"
	];
	
	/**
	 *  @var predefined set of words that can be associated with a chat bonus
	 */
	static $one_worders_set1 = ["Poop", "Yeah", "Blah", "Boo", "Fuuuuuuuu", "Shiiiiiiii"];
	static $one_worders_set2 = ["Ughhh", "Jeeezzz", "Sup", "Dawg", "Huhu", "Hehe"];
	static $one_worders_set3 = ["Dandanan", "Dude", "Where's", "my", "Car", "dooodooo"];
	static $one_worders_set4 = ["Bababa", "Babanana", "Noooooooooo", "Yesssssss", "soso", "lalalalala"];
	static $one_worders_set5 = ["I", "am", "hungry", "but", "gotta", "diet"];
	
	/**
	 *  @param [$number_of_words] -> integer number of words requested
	 *  @return array of string words 
	 */
	public static function getRandomWords($number_of_words)
	{
		$words = array();
		while($number_of_words > 0)
		{
			$random_index = mt_rand(0, count(self::$one_worders)-1);
			$word = self::$one_worders[$random_index];
			if(!in_array($word, $words, true))
			{
				$words[] = $word;
				$number_of_words--;
			}
		}
		return $words;
	}
}

?>