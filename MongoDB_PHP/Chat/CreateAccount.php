<?php

/** handles auto loading of file classes */
require_once __DIR__ . "/Autoloader/autoloader.php";

/**
 *  DEBUGGING MODE
 */
// turn this off for production test
// turn this on only for debugging
EchoResult::$result_mode = EchoResultMode::OFF;

/**
 *  INFORMATION SENT FROM CLIENT
 */
// get supplied chat user username from client
$chat_user_name = $_POST[ReferenceKeys::CHAT_USER_USERNAME];

/**
 *  SERVER CONNECTION AS ADMINISTRATOR
 */
// create a connection with server
$chat_server = new XMongoClient(ServerHostAddress::AWS_HOST);
$chat_server->connect(DBAccess::SUPERUSER_ADMIN_USERNAME, DBAccess::SUPERUSER_ADMIN_PASSWORD);

/**
 *  VERIFICATION IF USER ALREADY HAS A RECORD
 *  ACCOUNT CREATION 
 */
// get reference to chat database
$chat_users_db = $chat_server->selectDatabase(ReferenceKeys::DB_CHAT);

// get reference to playersInfo collection in chat database
$chat_users = $chat_users_db->selectCollection(ReferenceKeys::COLLECTION_CHAT_USERS);

// check if user exist in chat playersInfo
// if user doesn't exist add new user to chat playersInfo
$chat_user = $chat_users->findDocumentByName($chat_user_name);
if(is_null($chat_user))
{
	// create a new chat user with default starting values and unique username
	// add new user to playersInfo collection in chat database
	$new_user = new ChatUser();
	$new_user->addCredential(ReferenceKeys::CHAT_USER_USERNAME, $chat_user_name);
	$new_user->addCredential(ReferenceKeys::CHAT_USER_XP, ChatUserStartValues::START_XP);
	$new_user->addCredential(ReferenceKeys::CHAT_USER_BALANCE, ChatUserStartValues::START_BALANCE);
	$timeStamp = new DateTime("NOW", new DateTimeZone(TimeStampSettings::DATE_TIMEZONE));
	$new_user->addCredential(ReferenceKeys::CHAT_USER_TIMESTAMP, $timeStamp->format(DateTime::W3C));
	$new_user->addCredential(ReferenceKeys::CHAT_USER_DATE_TIME_JOINED, $timeStamp->format(DateTime::W3C));
	$new_user->addCredential(ReferenceKeys::CHAT_USER_CONSECUTIVE_DAYS_PLAYED, 0);
	$new_user->addCredential(ReferenceKeys::CHAT_USER_TOTAL_DAYS_PLAYED, 0);
	$new_user->addCredential(ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED, null);
	$chat_users->insertDocument($new_user->getFullUserCredentials());
	EchoResult::sendResult("<br><br> - new account created for " . $chat_user_name);
	EchoResult::sendResultOnce("success");
}
else
{
	EchoResult::sendResult("<br><br> - an account already exist with the name " . $chat_user_name . ".");
	EchoResult::sendResult("<br> - please pick a different username..");
	EchoResult::sendResultOnce("fail");
}

?>