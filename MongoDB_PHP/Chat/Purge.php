#! /usr/bin/php

<?php

$log_path = '/var/log/mongodb-purge.log';

//fork the process to work in a daemonized environment
file_put_contents($log_path, "Status: starting mongodb-purge...\n", FILE_APPEND);
$pid = pcntl_fork();
if($pid == -1)
{
    file_put_contents($log_path, "Error: mongodb-purge process failed...", FILE_APPEND);
    return 1; //error
}
else if($pid)
{
        return 0; //success
}
else
{
    /** handles auto loading of file classes */
    require_once __DIR__ . "/Autoloader/autoloader.php";

    $chat_server = new XMongoClient(ServerHostAddress::AWS_HOST);
    $chat_server->connect(DBAccess::SUPERUSER_ADMIN_USERNAME, DBAccess::SUPERUSER_ADMIN_PASSWORD);
    $chat_users_database = $chat_server->selectDatabase(ReferenceKeys::DB_CHAT);
    $chat_users = $chat_users_database->selectCollection(ReferenceKeys::COLLECTION_CHAT_USERS);
    $user_idle_limit = 480; // 480 seconds | 8 minutes
    $i = 0;
    while(true)
    {
        $log_message = "running... purging database of idle users... purge# " . strval($i) . "\n";
        file_put_contents($log_path, $log_message, FILE_APPEND);
        $documents = $chat_users->find(array(ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED => array( '$ne' => null )));
        $num_documents = count($documents);
	$purge_counter = 0;
        if($num_documents>0)
        {
            foreach ($documents as $elements)
            {
                $current_time = time();
                $last_timestamp = strtotime($elements[ReferenceKeys::CHAT_USER_TIMESTAMP]);
                $last_time_active = $current_time - $last_timestamp;
                if($last_time_active > $user_idle_limit)
                {
                    $chat_user_username = $elements[ReferenceKeys::CHAT_USER_USERNAME];
                    $room_connected = $elements[ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED];
                    $machine_name = explode(" ", $room_connected)[0];
                    $machine_db = $chat_server->selectDatabase($machine_name);
                    $machine_db_chatroom = $machine_db->selectCollection($room_connected);
                    $machine_db_chatroom->removeDocumentByName($chat_user_username);
                    $chat_users->updateDocumentByName($chat_user_username, ReferenceKeys::CHAT_USER_CURRENT_CHATROOM_JOINED, null);
		    $purge_counter++;
                }
            }
        }
        $log_message = $purge_counter . " idle users purged...\n";
        file_put_contents($log_path, $log_message, FILE_APPEND);
        $i++;
        sleep(600);
    }
}

?>

