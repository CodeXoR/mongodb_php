<?php

/**
 *  defined AWS Server Host Instance addresses
 *  <br><br><b>Note:</b> change values here if needed
 */
class ServerHostAddress
{
	/**
	 *  connection selection
	 *  unix domain socket --> /tmp/mongodb-sock:27017.sock
	 *  localhost --> 127.0.0.1:27017
	 *  primary and secondary replica sets --> ip-10-0-0-15:27017, ip-10-0-0-192:27017
	 */

	/**
	 *  @var constant string host address of AWS Server Instance
	 */
	const AWS_HOST = "mongodb://ip-10-0-0-15:27017,ip-10-0-0-192:27017";
}

?>