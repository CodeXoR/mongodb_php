<?php

/**
 *  constants used as uniform keys for inserting, removing, and updating documents in a collection
 */
class ReferenceKeys
{
	//------------------------------------ DATABASE ACCESS KEYS ------------------------------------//
	/**
	 *  @var constant string key of admin database
	 */
	const DB_ADMIN = "admin";
	/**
	 *  @var constant string key of chat database
	 */
	const DB_CHAT = "chat";
	/**
	 *  @var constant string key of machine database
	 */
	const DB_MACHINE = "machineName";
	
	//----------------------------------- COLLECTION ACCESS KEYS -----------------------------------//
	/**
	 *  @var constant string key of chat room collection
	 */
	const COLLECTION_CHAT_ROOM = "roomName";
	/**
	 *  @var constant string key of admin users collection
	 */
	const COLLECTION_ADMIN_USERS = "system.users";
	/**
	 *  @var constant string key of playersInfo collection in chat database
	 */
	const COLLECTION_CHAT_USERS = "playersInfo";
	
	//------------------------------------ DOCUMENT ACCESS KEYS ------------------------------------//
	/**
	 *  @var constant string key of database user unique username
	 */
	const CHAT_USER_ADMIN_USERNAME = "user";
	/**
	 *  @var constant string key of chat user unique id
	 */
	const CHAT_USER_ID = "_id";
	/**
	 *  @var constant string key of chat user unique username
	 */
	const CHAT_USER_USERNAME = "username";
	/**
	 *  @var constant string key of chat user password
	 */
	const CHAT_USER_PASSWORD = "password";
	/**
	 *  @var constant variable string key of current experience of chat user
	 */
	const CHAT_USER_XP = "xp";
	/**
	 *  @var constant variable string key of remaining balance of chat user
	 */
	const CHAT_USER_BALANCE = "balance";
	/**
	 *  @var constant variable string key of date and time chat user joined
	 */
	const CHAT_USER_DATE_TIME_JOINED = "dateTimeJoined";
	/**
	 *  @var constant variable string key of number of consecutive days played
	 */
	const CHAT_USER_CONSECUTIVE_DAYS_PLAYED = "consecutiveDaysPlayed";
	/**
	 *  @var constant variable string key of number of days played in total
	 */
	const CHAT_USER_TOTAL_DAYS_PLAYED = "totalDaysPlayed";
	/**
	 *  @var constant variable string key of boolean if chat user is currently in a chat room
	 */
	const CHAT_USER_CURRENT_CHATROOM_JOINED = "chatRoomJoined";
	/**
	 *  @var constant string key of chat user time stamp when accessing database
	 */
	const CHAT_USER_TIMESTAMP = "timeStamp";
}

?>