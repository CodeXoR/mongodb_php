<?php

/**
 *  special modifiers used for updating documents
 *  <br><br>References: <a href="http://docs.mongodb.org/manual/reference/operator/update/">MongoDB Update Operators</a>
 */
class UpdateOperators
{
	/**
	 * 	sets the value of a field in a document
	 */
	const SET = '$set';	
	/**
	 * 	removes the specified field from a document
	 */
	const DELETE = '$unset';
}

?>